#include "Robot.hpp"

#include "Buzzer.hpp"

Robot::Robot()
{
	m_buzzer = nullptr;
	m_irReceiver = nullptr;
	m_wheelController = nullptr;
	m_state = State_t::RC_CAR;
}

Robot::~Robot()
{
}

void
Robot::begin()
{
	if (m_buzzer != nullptr)
		m_buzzer->note(Buzzer::Notes::B3, 200, 100).
		note(Buzzer::Notes::B3, 200, 100).
		note(Buzzer::Notes::E3, 600, 50).
		relax();
}

void
Robot::step()
{
	if (m_irReceiver != nullptr) {
		Serial.println("Receiving...");
		IRReceiver::Buttons b = IRReceiver::Buttons::BUTTON_NONE;
		if (m_irReceiver->readIR(300, b)) {
			Serial.println(uint32_t(b), HEX);
			onButtonPressed(b);
		}
	}
}

void
Robot::setBuzzer(Buzzer* newVal)
{
	m_buzzer = newVal;
}

void
Robot::setIRReceiver(IRReceiver* newVal)
{
	m_irReceiver = newVal;
}

void
Robot::setWheelController(WheelsControl* newVal)
{
	m_wheelController = newVal;
}

void
Robot::onButtonPressed(IRReceiver::Buttons b)
{	
	static uint8_t leftQ = 30;
	
	switch (m_state) {
	case State_t::RC_CAR:
		
		switch (b) {
		case IRReceiver::Buttons::BUTTON_STAR:
			if (m_buzzer != nullptr) {
				m_buzzer->
				    note(Buzzer::Notes::C2, 300, 100).
				    note(Buzzer::Notes::C2, 300, 100).
				    relax();
			}
			m_state = State_t::LINE_FOLLOWER;
		case IRReceiver::Buttons::BUTTON_UP:
			if (m_wheelController != nullptr) {
				m_wheelController->setWheelsSpeed(60+leftQ, 60);
			}
			break;			
		case IRReceiver::Buttons::BUTTON_DOWN:
			if (m_wheelController != nullptr) {
				m_wheelController->setWheelsSpeed(-60-leftQ, -60);
			}
			break;
		case IRReceiver::Buttons::BUTTON_OK:
			if (m_wheelController != nullptr) {
				m_wheelController->setWheelsSpeed(0, 0);
			}
			break;
		case IRReceiver::Buttons::BUTTON_LEFT:
			if (m_wheelController != nullptr) {
				m_wheelController->setWheelsSpeed(-40-leftQ, 40);
			}
			break;
		case IRReceiver::Buttons::BUTTON_RIGHT:
			if (m_wheelController != nullptr) {
				m_wheelController->setWheelsSpeed(40+leftQ, -40);
			}
			break;
		default:
			break;
		}
		
		break;
	case State_t::LINE_FOLLOWER:
		
		switch (b) {
		case IRReceiver::Buttons::BUTTON_STAR:
			if (m_buzzer != nullptr) {
				m_buzzer->
				    note(Buzzer::Notes::C2, 300, 100).
				    note(Buzzer::Notes::C2, 300, 100).
				    note(Buzzer::Notes::C2, 300, 100).
				    relax();
			}
			m_state = State_t::OBSTACLE_AVOID;
		default:
			break;
		}
		
		break;
	case State_t::OBSTACLE_AVOID:
		
		switch (b) {
		case IRReceiver::Buttons::BUTTON_STAR:
			if (m_buzzer != nullptr) {
				m_buzzer->
				    note(Buzzer::Notes::C2, 300, 100).
				    relax();
			}
			m_state = State_t::RC_CAR;
		default:
			break;
		}
		
		break;
	default:
		break;
	}
}
