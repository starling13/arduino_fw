#define IR_SIGNAL_PIN 11
#define BUZZER_PIN 7

#include "Buzzer.hpp"
#include "IRReceiver.hpp"
#include "WheelsControl.hpp"
#include "Robot.hpp"

static Buzzer buzzer;
static IRReceiver irreceiver;
static Robot robot;
static WheelsControl wheelControl(5, 3, 12, 8, 2, 4);

void
setup()
{
	Serial.begin(115200);

	buzzer.begin(BUZZER_PIN);
	
	irreceiver.begin(IR_SIGNAL_PIN);

	robot.setBuzzer(&buzzer);
	robot.setIRReceiver(&irreceiver);
	robot.setWheelController(&wheelControl);
	robot.begin();
}

void
loop()
{
	robot.step();
}
