#include "WheelsControl.hpp"

WheelsControl::WheelsControl(
    uint8_t lpwm, uint8_t rpwm,
    uint8_t lfp,
    uint8_t lbp,
    uint8_t rfp,
    uint8_t rbp)
{
	m_leftPWMPin = lpwm;
	m_rightPWMPin = rpwm;
	
	m_leftForwardPin = lfp;
	m_leftBackwardPin = lbp;
	
	m_rightForwardPin = rfp;
	m_rightBackwardPin = rbp;
	
	pinMode(m_leftPWMPin, OUTPUT);
	pinMode(m_rightPWMPin, OUTPUT);
	
	pinMode(m_leftForwardPin, OUTPUT);
	pinMode(m_leftBackwardPin, OUTPUT);
	pinMode(m_rightForwardPin, OUTPUT);
	pinMode(m_rightBackwardPin, OUTPUT);
}

WheelsControl::~WheelsControl()
{
	
}

void WheelsControl::begin()
{
	
}

void WheelsControl::setWheelsSpeed(int8_t left, int8_t right)
{
	Serial.println(left, DEC);
	Serial.println(right, DEC);
	
	analogWrite(m_leftPWMPin, abs(left*2));
	analogWrite(m_rightPWMPin, abs(right*2));
	
	digitalWrite(m_leftForwardPin, left > 0 ? HIGH : LOW);
	digitalWrite(m_leftBackwardPin, left < 0 ? HIGH : LOW);
	digitalWrite(m_rightForwardPin, right > 0 ? HIGH : LOW);
	digitalWrite(m_rightBackwardPin, right < 0 ? HIGH : LOW);
}
