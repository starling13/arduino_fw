#ifndef ROBOT_HPP
#define ROBOT_HPP

#include "IRReceiver.hpp"
#include "WheelsControl.hpp"

class Buzzer;

class Robot
{
public:

	Robot();
	
	~Robot();

	void begin();

	void step();

	void setBuzzer(Buzzer* newVal);
	
	void setIRReceiver(IRReceiver* newVal);
	
	void setWheelController(WheelsControl* newVal);

private:
	
	enum class State_t
	{
		// RC car mode
		RC_CAR,
		// Program input mode
		PROGRAMMING,
		// 
		PROGRAM_FOLLOWER,
		
		LINE_FOLLOWER,
		
		OBSTACLE_AVOID
	};
	
	enum class ProgramCommands
	{
		NOP = 0,
		FORWARD,
		BACKWARD,
		LEFT,
		RIGHT,
		BEEP
	};
	
	struct ProgramLine
	{
		ProgramCommands command;
		uint8_t parameter;
	};
	
	void onButtonPressed(IRReceiver::Buttons);
	
	State_t m_state;
	
	ProgramLine m_program[32];
	
	Buzzer* m_buzzer;
	
	IRReceiver* m_irReceiver;
	
	WheelsControl* m_wheelController;
};

#endif // ROBOT_HPP

