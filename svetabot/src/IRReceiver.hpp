#ifndef IRRECEIVER_HPP
#define IRRECEIVER_HPP

#include <stdint.h>

#include "Controller.hpp"

class IRReceiver : public Controller
{
public:

	enum Buttons : uint32_t
	{
		BUTTON_NONE = 0,
		BUTTON_1 = 0xFF00BF00,
		BUTTON_2 = 0xFE01BF00,
		BUTTON_3 = 0xFD02BF00,
		BUTTON_4 = 0xFB04BF00,
		BUTTON_5 = 0xFA05BF00,
		BUTTON_6 = 0xF906BF00,
		BUTTON_7 = 0xF708BF00,
		BUTTON_8 = 0xF609BF00,
		BUTTON_9 = 0xF50ABF00,
		BUTTON_0 = 0xF20DBF00,
		BUTTON_STAR = 0xF30CBF00,
		BUTTON_GRID = 0xF10EBF00,
		BUTTON_UP = 0xEE11BF00,
		BUTTON_DOWN = 0xE619BF00,
		BUTTON_LEFT = 0xEB14BF00,
		BUTTON_RIGHT = 0xE916BF00,
		BUTTON_OK = 0xEA15BF00
	};

	void begin(uint8_t);

	bool readIR(uint16_t dl, Buttons& d);

private:

	bool readBit(bool& val);

	uint8_t m_pin;
};

#endif // IRRECEIVER_HPP

