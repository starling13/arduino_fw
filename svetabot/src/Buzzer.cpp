#include "Buzzer.hpp"

Buzzer::Buzzer()
{
}

Buzzer::~Buzzer()
{
}

void
Buzzer::begin(uint8_t pin)
{
	m_pin = pin;
	pinMode(m_pin, OUTPUT);
	digitalWrite(m_pin, HIGH);
}

void Buzzer::relax()
{
	digitalWrite(m_pin, HIGH);
}

Buzzer&
Buzzer::note(Notes freq, uint16_t len, uint16_t del)
{
	tone(m_pin, unsigned(freq), len);
	delay(del+len);
	return *this;
}

