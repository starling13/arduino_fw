#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

class Robot;

class Controller
{
public:

	void setRobot(Robot* newVal);

protected:

	Robot* m_robot;
};

class SerialInterface : public Controller
{
public:
};

#endif // CONTROLLER_HPP

