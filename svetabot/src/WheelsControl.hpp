#ifndef WHEELSCONTROL_HPP
#define WHEELSCONTROL_HPP

#include <stdint.h>


class WheelsControl
{
public:
	
	WheelsControl(
	    uint8_t lpwmPin,
	    uint8_t rpwmPin,
	    uint8_t lfp,
	    uint8_t lbp,
	    uint8_t rfp,
	    uint8_t rbp);
	
	~WheelsControl();
	
	void begin();
	
	void setWheelsSpeed(int8_t left, int8_t right);
	
private:

	uint8_t m_leftPWMPin;
	
	uint8_t m_rightPWMPin;
	
	uint8_t m_leftForwardPin;
	
	uint8_t m_leftBackwardPin;
	
	uint8_t m_rightForwardPin;
	
	uint8_t m_rightBackwardPin;
};

#endif /* WHEELSCONTROL_HPP */

