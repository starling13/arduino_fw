#include "IRReceiver.hpp"

void
IRReceiver::begin(uint8_t pin)
{
	m_pin = pin;
	pinMode(m_pin, INPUT);
}

bool IRReceiver::readIR(uint16_t dl, Buttons& d)
{
	// IDLE

	uint32_t pause = dl * 1000ul;
	while (digitalRead(11)) {
		delayMicroseconds(100);
		if (pause < 100)
			return false;
		pause -= 100;
	}

	// START

	uint32_t len = 0;
	while (!digitalRead(11)) {
		delayMicroseconds(50);
		len += 50;
	}

	if ((len < 8000) || (len > 9000))
		return false;

	// START low

	len = 0;
	while (digitalRead(11)) {
		delayMicroseconds(50);
		len += 50;
	}

	if ((len < 4000) || (len > 4500))
		return false;

	bool data[32];

	bool b;
	uint8_t i = 0;
	while (readBit(b)) {
		data[i++] = b;
	}

	d = 0;
	for (uint8_t j = 0; j < i; ++j) {
		if (data[j])
			d |= (1l << j);
	}

	return true;
}

bool IRReceiver::readBit(bool& val)
{
	uint32_t len = 0;
	while (!digitalRead(11)) {
		delayMicroseconds(50);
		len += 50;
		if (len > 650)
			return false;
	}

	len = 0;
	while (digitalRead(11)) {
		delayMicroseconds(50);
		len += 50;
		if (len > 4000)
			return false;
	}

	val = (len > 650);
	return true;
}
