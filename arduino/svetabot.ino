#define IR_SIGNAL_PIN 11
#define BUZZER_PIN 7

class Buzzer
{
public:

  enum Notes : uint16_t
  {
    C1 = 33,  // 32.70
    D1 = 37,  // 36.71
    E1 = 41,  // 41.20
    F1 = 44,  // 43.65
    G1 = 49,  // 49.00
    A1 = 55,  // 55.00
    B1 = 62,  // 61.74
    
    C2 = 65,  // 65.41
    D2 = 73,  // 73.42
    E2 = 82,  // 82.41
    F2 = 87,  // 87.31
    G2 = 98,  // 98.00
    A2 = 110, // 110.00
    B2 = 123, // 123.47
    
    C3 = 131, // 130.81
    D3 = 147, // 146.83
    E3 = 165, // 164.81
    F3 = 175, // 174.61
    G3 = 196, // 196.00
    A3 = 220, // 220.00
    B3 = 247, // 246.94

    C4 = 262, // 261.63
    D4 = 294, // 293.66
    E4 = 330, // 329.63
    F4 = 349, // 349.23
    G4 = 392, // 392.00
    A4 = 440, // 440.00
    B4 = 494, // 493.88

    C5 = 523, // 523.25
    D5 = 587, // 587.33
    E5 = 659, // 659.25  
    F5 = 698, // 698.46
    G5 = 784, // 783.99
    A5 = 880, // 880.00
    B5 = 988, // 987.77

    C6 = 1047, // 1046.50
    D6 = 1175, // 1174.66
    E6 = 1319  // 1318.51
  };

  Buzzer();
  ~Buzzer();

  void begin(uint8_t pin);

  Buzzer& note(uint16_t freq, uint16_t len, uint16_t del);

private:

  uint8_t m_pin;
};

Buzzer::Buzzer()
{
}

Buzzer::~Buzzer()
{
}

void Buzzer::begin(uint8_t pin)
{
    m_pin = pin;
    pinMode(m_pin, OUTPUT);
    digitalWrite(m_pin, HIGH);
}

Buzzer& Buzzer::beep(uint16_t freq, uint16_t len, uint16_t del)
{
  tone(m_pin, freq, len);
  delay(del);
  return *this;
}

class Controller
{
public:

  void setRobot(Robot* newVal);
  
protected:

  Robot* m_robot;
};

void Controller::setRobot(Robot* newVal)
{
  
}

class IRReceiver : public Controller
{
public:

  
};

class SerialInterface : public Controller
{
public:
};

class Robot
{
public:

  Robot();
  ~Robot();

  void begin();

  void step();

  void setBuzzer(Buzzer* newVal)
  {
    m_buzzer = newVal;
  }
  
private:

  Buzzer* m_buzzer;
};

Robot::Robot()
{
  m_buzzer = nullptr;
}

Robot::~Robot()
{
}

void Robot::begin()
{
  if (m_buzzer != nullptr)
    m_buzzer->beep(440, );
}

void Robot::step()
{
}

static Buzzer buzzer;
static Robot robot;

void setup()
{
  pinMode(IR_SIGNAL_PIN, INPUT);

  Serial.begin(115200);

  buzzer.begin(BUZZER_PIN);

  robot.setBuzzer(&buzzer);
  robot.begin();
}

void loop()
{
  robot.step();
}
